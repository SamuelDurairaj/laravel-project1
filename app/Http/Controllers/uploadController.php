<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\location;

class uploadController extends Controller
{
    public function index()
    {
          return view('upload.upload'); //upload.upload.blade.php

    }

    public function locationstore(request $request)
    {
        //get file
        $upload = $request->file('upload_file');
        $filePath=$upload->getRealPath();

        //open and read
        $file=fopen($filePath, 'r');
        $header=fgetcsv($file);

        //dd($header);
        $escapedHeader=[];
        //validate

        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/','', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //dd($escapedHeader);

        //looping through the other columns
        while ($columns=fgetcsv($file)) {
           if($columns[0]=="")
           {
            continue;
           }

           //trim data
           /*foreach ($columns as $key => &$value) {
               $value=preg_replace('/\D/', '', $value);
           }*/
           $data=array_combine($escapedHeader, $columns);

           /*foreach ($data as $key => &$value) {
               $value=($key=="type"||$key=="gln")?(text)$value:(text)$value;
           }*/
            //Table Update

           $type=$data['type'];
           $gln=$data['gln'];
           $name=$data['name'];
           $emailaddress=$data['emailaddress'];
           $address=$data['address'];
           $city=$data['city'];
           $state=$data['state'];
           $zip=$data['zip'];
           $phone=$data['phone'];
           $country=$data['country'];

           $location= location::firstOrNew(['type'=>$type,'gln'=>$gln]);
           $location->name=$name;
           $location->emailaddress=$emailaddress;
           $location->address=$address;
           $location->city=$city;
           $location->state=$state;
           $location->zip=$zip;
           $location->phone=$phone;
           $location->country=$country;
           $location->save();

        }


    }

    Back Up for Laravel Project1 - uploadController.php
---------------------------------------------------


/*public function store(request $request)
    {
        //get file
        $upload = $request->file('upload_file');
        $filePath=$upload->getRealPath();

        //open and read
        $file=fopen($filePath, 'r');
        $header=fgetcsv($file);

        //dd($header);
        $escapedHeader=[];
        //validate

        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/','', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //dd($escapedHeader);

        //looping through the other columns
        while ($columns=fgetcsv($file)) {
           if($columns[0]=="")
           {
            continue;
           }

           //trim data
           foreach ($columns as $key => &$value) {
               $value=preg_replace('/\D/', '', $value);
           }
           $data=array_combine($escapedHeader, $columns);

           foreach ($data as $key => &$value) {
               $value=($key=="zip"||$key=="month")?(integer)$value:(float)$value;
           }
            //Table Update

           $zip=$data['zip'];
           $month=$data['month'];
           $lodging=$data['lodging'];
           $meal=$data['meal'];
           $housing=$data['housing'];

           $uploadedFile= uploadedFile::firstOrNew(['zip'=>$zip,'month'=>$month]);
           $uploadedFile->lodging=$lodging;
           $uploadedFile->meal=$meal;
           $uploadedFile->housing=$housing;
           $uploadedFile->save();

        }
    }*/




/*public function store(request $request)
{    
    if($request->hasFile('upload_file'))
    {
    $request->file('upload_file');
    return $request->upload_file->extension();
    return $request->upload_file->path();
    return $request->upload_file->store('public');
    return Storage::putFile('public',$request->file('upload_file'));
   }
}*/
    
}