<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class uploadedFile extends Model
{
    protected $table="fileuploads";
    public $fillable = ['zip','month','lodging','meal','housing'];
}
