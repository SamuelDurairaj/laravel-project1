<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class location extends Model
{
    protected $table="Alocations";
    public $fillable = ['type','gln','name','emailaddress','address','city','state','zip','phone','country'];
}
