<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Upload Functionality</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
  </head>
  <body>
    <br>
    <div class="col-lg-offset-4 col-lg-4">
      <center><h1>UPLOAD FILE</h1></center>
    <form action="{{URL('/locationstore')}}" enctype="multipart/form-data" method="post">
      {{csrf_field()}}
      <input type="file" name="upload_file">
      <br>
      <input type="submit" value="Upload">
    </form>
    </div>
  </body>
</html>