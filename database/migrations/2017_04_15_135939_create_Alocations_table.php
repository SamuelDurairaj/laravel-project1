<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Alocations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type');
            $table->text('gln');
            $table->text('name');
            $table->text('emailaddress');
            $table->text('address');
            $table->text('city');
            $table->text('state');
            $table->integer('zip');
            $table->integer('phone');
            $table->text('country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Alocations');
    }
}
